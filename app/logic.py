from collections import Counter
import json
from app.forms import urls
from google.appengine.api import urlfetch
from spell_checker import correct


def get_data(target):
    if target not in urls.keys():
        return
    r = urlfetch.fetch(url='http://data.gov.spb.ru/api/v1/datasets/%d/versions/latest/data' % target,
                       headers={"Authorization": "Token 95f12004f6ed675c3105da3695fee98b88b7195e"},
                       allow_truncated=True,
                       deadline=60)
    return json.loads(r.content)


def determine_key(row):
    for i in ['district', 'district_city', 'region', 'area', 'raion', 'addr_district', 'disrict', 'note']:
        if i in row.keys():
            return i


def analyze(data):
    data = map(lambda x: x['row'], data)
    key = determine_key(data[0])
    d = []
    for i in data:
        ob = i.get(key)
        if ob:
            s = ob.split()
            for ob in s:
                ob = correct(ob.capitalize())
                if ob:
                    d.append(ob)
                    break
    return Counter(d)


def merge(x, y):
    return [{'name': i, 'x': x.get(i) or 0, 'y': y.get(i) or 0} for i in set(x.keys() + y.keys())]
