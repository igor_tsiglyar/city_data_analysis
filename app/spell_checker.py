# -*- coding: utf-8 -*-

import re
import collections


def words(text):
    return re.findall('[a-z]+', text.lower())


def train(features):
    model = collections.defaultdict(lambda: 1)
    for f in features:
        model[f] += 1
    return model


DISTRICTS = [u'Адмиралтейский', u'Василеостровский', u'Выборгский', u'Калининский', u'Кировский',
             u'Колпинский', u'Красногвардейский', u'Красносельский', u'Кронштадтский', u'Курортный',
             u'Московский', u'Невский', u'Петроградский', u'Петродворцовый', u'Приморский', u'Пушкинский',
             u'Фрунзенский', u'Центральный']


replace = {u'Колпино': u'Колпинский',
           u'Петергоф': u'Петродворцовый',
           u'Сестрорецк': u'Курортный',
           u'Зеленогорск': u'Курортный',
           u'Кронштадт': u'Кронштадтский',
           u'Ломоносовский': u'Петродворцовый',
           u'Ломоносов': u'Петродворцовый',
           u'Центр': u'Центральный'}


NWORDS = train(DISTRICTS + replace.keys())
alphabet = u'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'


def edits(word):
    s = [(word[:i], word[i:]) for i in range(len(word) + 1)]
    deletes = [a + b[1:] for a, b in s if b]
    transposes = [a + b[1] + b[0] + b[2:] for a, b in s if len(b) > 1]
    replaces = [a + c + b[1:] for a, b in s for c in alphabet if b]
    inserts = [a + c + b for a, b in s for c in alphabet]
    return set(deletes + transposes + replaces + inserts)


def known(words):
    return set(w for w in words if w in NWORDS)


def correct(word):
    candidates = known([word]) or known(edits(word))
    if not candidates:
        return
    ans = max(candidates, key=NWORDS.get)
    return replace[ans] if ans in replace else ans

