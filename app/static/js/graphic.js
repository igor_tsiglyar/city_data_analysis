var margin = {
    top: 20,
    right: 80,
    bottom: 30,
    left: 80
}
var width = 1000 - margin.left - margin.right
var height = 400 - margin.top - margin.bottom

function loadPage() {
    console.log("loaded")
}


function test_this(par) {
    var d = jQuery.parseJSON(par)
    console.log(typeof(d))
    console.log(d);
}


function print_diagram(par) {
    var dat = jQuery.parseJSON(par)
    console.log(dat)

    d3.select("svg")
        .remove()

    var svg = d3.select("body").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    var x = d3.scale.linear()
        .range([0, width - 100])

    var y = d3.scale.linear()
        .range([height, 0])

    var g = svg.selectAll('g.point')
        .data(dat)
        .enter()
        .append('g')
        .attr('class', 'point')

    x.domain(d3.extent(dat, function(d) {
        return Number(d.x)
    }))
    y.domain(d3.extent(dat, function(d) {
        return Number(d.y)
    }))

    svg.append('g')
        .attr('transform', 'translate(0,' + height + ')')
        .call(d3.svg.axis()
            .scale(x)
            .orient('bottom'))

    svg.append('g')
        .attr('transform', 'translate(0, 0)')
        .call(d3.svg.axis()
            .scale(y)
            .orient('left'))

    var size = 14

    var popup = svg.append('text')




    g.append('circle')
        .attr('r', 4)
        .attr('fill', "Blue")
        .attr('cx', function(d) {
            return x(Number(d.x))
        })
        .attr('cy', function(d) {
            return y(Number(d.y))
        })
        .on('mouseover', function(d, i) {
            popup.text(d.name)
                .attr('x', x(d.x))
                .attr('y', y(d.y))
                .attr('dx', 5)
                .attr('dy', -5)

        })
        .on('mouseout', function(d, i) {
            popup.text('')
        })




}

function print_countries() {

    d3.select("svg")
        .remove()


    var svg = d3.select("body").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    var x = d3.scale.linear()
        .range([0, width])

    var y = d3.scale.linear()
        .range([height, 0])

    var r = d3.scale.linear()
        .range([3, 10])

    d3.csv('http://d3-js.ru/data/gapminder.csv', function(country) {
        console.log(country)
        country.gdp = Number(country.gdp)
        country.life = Number(country.life)
        country.population = Number(country.population)
        console.log(typeof(country))
        return country
    }, function(countries) {

        var g = svg.selectAll('g.point')
            .data(countries)
            .enter()
            .append('g')
            .attr('class', 'point')

        x.domain(d3.extent(countries, function(d) {
            return d.gdp
        }))
        y.domain(d3.extent(countries, function(d) {
            return d.life
        }))
        r.domain(d3.extent(countries, function(d) {
            return d.population
        }))

        svg.append('g')
            .attr('transform', 'translate(0,' + height + ')')
            .call(d3.svg.axis()
                .scale(x)
                .orient('bottom'))

        svg.append('g')
            .call(d3.svg.axis()
                .scale(y)
                .orient('left'))



        g.append('circle')
            .attr('r', function(d) {
                return r(d.population)
            })
            .attr('fill', function(d) {
                return d.color
            })
            .attr('cx', function(d) {
                return x(d.gdp)
            })
            .attr('cy', function(d) {
                return y(d.life)
            })

        g.append('text')
            .attr('dx', 10)
            .attr('dy', 5)
            .text(function(d) {
                return d.country
            })
            .attr('x', function(d) {
                return x(d.gdp)
            })
            .attr('y', function(d) {
                return y(d.life)
            })

    })
}