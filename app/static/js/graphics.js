var margin = { top: 40,
               right: 150,
               bottom: 60,
               left: 50}
var width = 1050 - margin.left - margin.right
var height = 500 - margin.top - margin.bottom

function loadPage() {
    //print_diagram(par)
}

function test_this(par) {
    var d = jQuery.parseJSON(par)
    console.log(typeof(d))
    console.log(d);
}

function print_diagram(par) {
    var dat = jQuery.parseJSON(par)
    var points = dat['points']
    //console.log(dat)

    d3.select("svg")
        .remove()

    var svg = d3.select("div").select("div").select("div").select("div").append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    var x = d3.scale.linear()
        .range([0, width])

    var y = d3.scale.linear()
        .range([height, 0])

    var g = svg.selectAll('g.point')
        .data(points)
        .enter()
        .append('g')

    x.domain(d3.extent(points, function(d) {
        return Number(d.x)
    }))
    y.domain(d3.extent(points, function(d) {
        return Number(d.y)
    }))

    svg.append('g')
        .attr('transform', 'translate(0,' + height + ')')
        .call(d3.svg.axis()
            .scale(x)
            .orient('bottom'))

    svg.append('g')
        .attr('transform', 'translate(0, 0)')
        .call(d3.svg.axis()
            .scale(y)
            .orient('left'))

    g.append('text')
        .text(dat['x_name'])
        .attr('x', width - 8 * dat['x_name'].length)
        .attr('y', height + 35)

    g.append('text')
        .text(dat['y_name'])
        .attr('x', -margin.left / 2)
        .attr('y', -margin.top / 2)

    var popup = svg.append('text')

    g.append('circle')
        .attr('r', 4)
        .attr('fill', "Blue")
        .attr('cx', function(d) {
            return x(Number(d.x))
        })
        .attr('cy', function(d) {
            return y(Number(d.y))
        })
        .on('mouseover', function(d, i) {
            var t = []
            for (var i = points.length - 1; i >= 0; i--) {
                if (x(points[i]['x']) - 3 < x(d.x) && x(d.x) < x(points[i]['x']) + 3 && y(points[i]['y']) - 3 < y(d.y) && y(d.y) < y(points[i]['y']) + 3){
                    //console.log(x(d.x), y(d.y), ':', points[i]['name'], x(points[i]['x']), y(points[i]['y']))
                    t.push(points[i]['name'])
                }
            };
            console.log('_______')
            popup.text(t.join(', '))
                .attr('x', x(d.x))
                .attr('y', y(d.y))
                .attr('dx', 4)
                .attr('dy', -5)

        })
        .on('mouseout', function(d, i) {
            popup.text('')
        })
}