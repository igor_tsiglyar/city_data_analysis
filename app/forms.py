from flask.ext.wtf import Form
from wtforms import SelectField

urls = {4:		'Cultural heritage',
        9:		'Road works',
        10:		'Transport movement restrictions',
        11:		'Work orders',
        13:		'Project planning areas',
        31:		'Sport objects',
        32:		'Industrial enterprises',
        44:		'Civil engineering',
        61:		'Hotels',
        64:		'Libraries',
        69:		'Appartment repairs',
        78:		'Immovables',
        72:		'Educational institutions of culture',
        82:		'Sport education centers for children',
        89:		'Social pharmacies',
        90:		'Hospitals',
        91:		'Social organizations',
        92:		'Post offices',
        95:     'Pharmacies',
        113:	'Departments of housing subsidies',
        123:	'Museums',
        124: 	'Theaters',
        125: 	'Showrooms',
        126: 	'Cinemas',
        127: 	'Restaurants',
        128: 	'Sights',
        131:	'"Citizen-Police" Terminals'}

choices = urls.items()
choices.sort(key=lambda x: x[1])


class SelectForm(Form):
    x_field = SelectField(choices=choices)
    y_field = SelectField(choices=choices)