import json
from flask import render_template, flash, request
from app.logic import get_data, analyze, merge
from app.forms import SelectForm, urls


def index():
    x_name = int(request.args.get('x_field') or 0)
    y_name = int(request.args.get('y_field') or 0)
    form = SelectForm(x_field=x_name, y_field=y_name)
    d = None
    if x_name == y_name:
        flash('Choose different stats')
    else:
        x = get_data(x_name)
        y = get_data(y_name)

        if x and y:
            x = analyze(x)
            y = analyze(y)
            d = {'x_name': urls.get(x_name), 'y_name': urls.get(y_name), 'points': merge(x, y)}
    return render_template("index.html", form=form, d=[json.dumps(d)])
